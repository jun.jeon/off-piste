from logging import getLogger

from flask import Blueprint, jsonify

api_name = 'check-in'
url_prefix = '/{}'.format(api_name)

# [Project Gnar]: This variable must be `blueprint` for compatibility with `gnar-gear`
blueprint = Blueprint(api_name, __name__, url_prefix=url_prefix)

log = getLogger(__name__)


@blueprint.route('', methods=['GET'])
def check_in():
    return jsonify('Off-Piste checking in!')
