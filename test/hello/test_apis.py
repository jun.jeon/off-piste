import logging

import pytest
from gnar_gear import GnarApp

from ..constants import ENVIRONMENT

log = logging.getLogger()


class TestApis:

    @pytest.fixture
    def mock_app(self, monkeypatch):
        monkeypatch.setattr('argparse.ArgumentParser.parse_args',
                            lambda *a, **kw: type('args', (object,), {'port': ''}))
        monkeypatch.setattr('flask.Flask.run', lambda *a, **kw: None)
        monkeypatch.setattr('os.environ', ENVIRONMENT)
        monkeypatch.setattr('postgres.Postgres.__init__', lambda *a, **kw: None)
        app = GnarApp('off-piste', production=False, port=9400, no_db=True, no_jwt=True)
        app.run()
        yield app.flask.test_client()

    def test_10_45(self, monkeypatch, mock_app):
        response = mock_app.get('/check-in')
        assert response.json == 'Off-Piste checking in!'
